## Basic config in one place
All of this stuff must be in a **home** directory except files in **include** directory
This files must be located according to the list


## include lists

### kbdlayout 
```
/usr/share/i3blocks/kbdlayout (deprecated)
```

### xkblayout-state
```
path: /bin/xkblayout-state
sourcecode: https://github.com/nonpop/xkblayout-state.git
installation requires:
build-essentials

```
