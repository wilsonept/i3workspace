set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim " path for Vundle files (plugin manager)

call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'pprovost/vim-ps1'
call vundle#end()

filetype plugin indent on

set number
syntax on
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set softtabstop=4

set autoindent
let python_highlight_all = 1
set t_Co=256

set mouse=a
set termencoding=utf-8
set novisualbell
set t_vb=
set backspace=indent,eol,start whichwrap+=<,>,[,]
set showtabline=1
set wrap
set linebreak
set clipboard=unnamed
set ruler
set visualbell t_vb=
